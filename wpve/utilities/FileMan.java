package wpve.utilities;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.file.YamlConfiguration;

import wpve.core.Arena;
import wpve.core.Main;
import wpve.core.Wave;

import static wpve.core.Main.ARENAS;
import wpve.utilities.StringUtils;

public class FileMan {
	
	File ReadMe;
	File arenaFile;
	File waveFile;
	
	YamlConfiguration arenaYml;
	YamlConfiguration waveYml;
	
	public void loadFiles(File dataFolder){

		ReadMe = new File(dataFolder, "ReadMe.txt");
		arenaFile = new File(dataFolder, "Arenas.yml");
		waveFile = new File(dataFolder, "Waves.yml");
		
		if(!arenaFile.exists()){
			new File(dataFolder + "").mkdir();
			
			try {
				if(arenaFile.createNewFile()){
					try {
						PrintWriter writer = new PrintWriter(arenaFile);
						
						writer.write( "" );
						writer.close();
						
					} catch (IOException e2) {
						// TODO Auto-generated catch block
						e2.printStackTrace();
					}
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
			}
		}
				
		if(!ReadMe.exists()){
			ReadMeGen.generator(ReadMe);
		}
		
		if(!waveFile.exists()){
			new File(dataFolder + "").mkdir();
			
			try {
				if(waveFile.createNewFile()){
					try {
						PrintWriter writer = new PrintWriter(waveFile);
						
						writer.write( "" );
						writer.close();
						
					} catch (IOException e2) {
						// TODO Auto-generated catch block
						e2.printStackTrace();
					}
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
			}
		}
		
		this.arenaYml = YamlConfiguration.loadConfiguration(arenaFile);
		this.waveYml = YamlConfiguration.loadConfiguration(waveFile);
		
		try {
			Set<String> arenaList = arenaYml.getKeys(false);
			for(String id : arenaList){
				Arena arena = new Arena();
			    arena.setId(id);
			    arena.setName(arenaYml.getString(id + ".Name"));
			
			    String worldname = arenaYml.getString(id + ".Location.World");
			    double x, y, z;
			    x = arenaYml.getDouble(id + ".Location.X");
			    y = arenaYml.getDouble(id + ".Location.Y");
			    z = arenaYml.getDouble(id + ".Location.Z");
			    arena.setLocation( new Location(Bukkit.getWorld(worldname), x, y, z));
			    
			    ArrayList<Location> SpawnLocations = new ArrayList<Location>();
			    
			    for(int i = 0; i < arenaYml.getInt(id + ".NumSpawnLocations"); i++){
			    	x = arenaYml.getDouble(id + ".SpawnLocations." + i + ".X");
				    y = arenaYml.getDouble(id + ".SpawnLocations." + i + ".X");
				    z = arenaYml.getDouble(id + ".SpawnLocations." + i + ".X");
				    
				    Location loc = new Location(Bukkit.getWorld(worldname), x, y, z);
				    
				    SpawnLocations.add(loc);
			    }
			    
			    arena.setSpawnLocations(SpawnLocations);
			    
			    Main.ArenaList.add(arena);
			    Main.ARENAS.put(id, arena);
			}
		}
		catch (NullPointerException e) {
			//Doesn't Exist Yet
		}
		
		try {
			Set<String> waveList = waveYml.getKeys(false);
			for(String id : waveList){
				Wave wave = new Wave();
			    wave.setId(Integer.parseInt(id));
			    int idInt = Integer.parseInt(id);

			    HashMap<String, Integer> Count = new HashMap<String, Integer>();
				    Count.put("Blaze", waveYml.getInt(id + ".Count.Blaze"));
				    Count.put("Creeper", waveYml.getInt(id + ".Count.Creeper"));
				    Count.put("Endermite", waveYml.getInt(id + ".Count.Endermite"));
				    Count.put("Evoker", waveYml.getInt(id + ".Count.Evoker"));
				    Count.put("Ghast", waveYml.getInt(id + ".Count.Ghast"));
				    Count.put("Gaurdian", waveYml.getInt(id + ".Count.Gaurdian"));
				    Count.put("Magma_Cube", waveYml.getInt(id + ".Count.Magma_Cube"));
				    Count.put("Silverfish", waveYml.getInt(id + ".Count.Silverfish"));
				    Count.put("Skeleton", waveYml.getInt(id + ".Count.Skeleton"));
				    Count.put("Slime", waveYml.getInt(id + ".Count.Slime"));
				    Count.put("Stray", waveYml.getInt(id + ".Count.Stray"));
				    Count.put("Vex", waveYml.getInt(id + ".Count.Vex"));
				    Count.put("Vindicator", waveYml.getInt(id + ".Count.Vindicator"));
				    Count.put("Witch", waveYml.getInt(id + ".Count.Witch"));
				    Count.put("WitherSkeleton", waveYml.getInt(id + ".Count.WitherSkeleton"));
				    Count.put("Zombie", waveYml.getInt(id + ".Count.Zombie"));	
				wave.setCount(Count);
				    
				HashMap<String, Integer> Level = new HashMap<String, Integer>();
					Level.put("Blaze", waveYml.getInt(id + ".Level.Blaze"));
					Level.put("Creeper", waveYml.getInt(id + ".Level.Creeper"));
					Level.put("Endermite", waveYml.getInt(id + ".Level.Endermite"));
					Level.put("Evoker", waveYml.getInt(id + ".Level.Evoker"));
					Level.put("Ghast", waveYml.getInt(id + ".Level.Ghast"));
					Level.put("Gaurdian", waveYml.getInt(id + ".Level.Gaurdian"));
					Level.put("Magma_Cube", waveYml.getInt(id + ".Level.Magma_Cube"));
					Level.put("Silverfish", waveYml.getInt(id + ".Level.Silverfish"));
					Level.put("Skeleton", waveYml.getInt(id + ".Level.Skeleton"));
					Level.put("Slime", waveYml.getInt(id + ".Level.Slime"));
					Level.put("Stray", waveYml.getInt(id + ".Level.Stray"));
					Level.put("Vex", waveYml.getInt(id + ".Level.Vex"));
					Level.put("Vindicator", waveYml.getInt(id + ".Level.Vindicator"));
					Level.put("Witch", waveYml.getInt(id + ".Level.Witch"));
					Level.put("WitherSkeleton", waveYml.getInt(id + ".Level.WitherSkeleton"));
					Level.put("Zombie", waveYml.getInt(id + ".Level.Zombie"));
				wave.setLevel(Level); 
			    
			    Main.WaveList.add(wave);
			    Main.WAVES.put(idInt, wave);
			}
			
			for (int i = 0; i < 405; i++){
				if(Main.WAVES.get(i) == null){
					createWave(i);
				}
			}
		}
		catch (NullPointerException e) {
			//Doesn't Exist Yet
		}
	}
	
	public void saveFile(){
		StringUtils.printInfo("Saving data...");
		boolean error = false;

		try {
			arenaYml.save(arenaFile);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			error = true;
		}
		
		try{
			waveYml.save(waveFile);
		} catch (IOException e) {
			e.printStackTrace();
			error = true;
		}
		
		if (error) {
			StringUtils.printError("Data save, was unsuccessful, check logs");
		} else {
			StringUtils.printInfo("Success!");
		}
	}
	
	public void createArena(String id, Location loc){
		Arena arena = new Arena();
	    arena.setId(id);
	    arena.setName(id);
	
	    String worldname = loc.getWorld().getName();
	    double x, y, z;
	    x = loc.getX();
	    y = loc.getY();
	    z = loc.getZ();
	    arena.setLocation( new Location(Bukkit.getWorld(worldname), x, y, z));
	    Main.ArenaList.add(arena);
	    ARENAS.put(id, arena);
	    saveArena(arena);
	}
	
	public void createWave(int id){
		
		Wave wave = new Wave();

		wave.setId(id);
		
		HashMap<String, Integer> Count = new HashMap<String, Integer>();
		Count.put("Blaze", 0);
		Count.put("Creeper", 0);
		Count.put("Endermite", 0);
		Count.put("Evoker", 0);
		Count.put("Ghast", 0);
		Count.put("Gaurdian", 0);
		Count.put("Magma_Cube", 0);
		Count.put("Silverfish", 0);
		Count.put("Skeleton", 0);
		Count.put("Slime", 0);
		Count.put("Stray", 0);
		Count.put("Vex", 0);
		Count.put("Vindicator", 0);
		Count.put("Witch", 0);
		Count.put("WitherSkeleton", 0);
		Count.put("Zombie", 0);		
		
		HashMap<String, Integer> Level = new HashMap<String, Integer>();
		Level.put("Blaze", 0);
		Level.put("Creeper", 0);
		Level.put("Endermite", 0);
		Level.put("Evoker", 0);
		Level.put("Ghast", 0);
		Level.put("Gaurdian", 0);
		Level.put("Magma_Cube", 0);
		Level.put("Silverfish", 0);
		Level.put("Skeleton", 0);
		Level.put("Slime", 0);
		Level.put("Stray", 0);
		Level.put("Vex", 0);
		Level.put("Vindicator", 0);
		Level.put("Witch", 0);
		Level.put("WitherSkeleton", 0);
		Level.put("Zombie", 0);
		
		wave.setCount(Count);
		wave.setLevel(Level);
		
		Main.WaveList.add(wave);
	    Main.WAVES.put(id, wave);
	}
	
	public void saveArena(Arena arena){
		
	    arenaYml.set(arena.getId() + ".Name", arena.getName());
	    arenaYml.set(arena.getId() + ".Location.World", arena.getLocation().getWorld().getName());
		arenaYml.set(arena.getId() + ".Location.X", arena.getLocation().getX());
	    arenaYml.set(arena.getId() + ".Location.Y", arena.getLocation().getY());
	    arenaYml.set(arena.getId() + ".Location.Z", arena.getLocation().getZ());
	    
	    int i = 0;
	    ArrayList<Location> spawnLocations = arena.getSpawnLocations();
	    for(; i < spawnLocations.size() ; i++){
	    	Location loc = spawnLocations.get(i);
	    	arenaYml.set(arena.getId() + ".SpawnLocations." + i + ".X", loc.getX());
	    	arenaYml.set(arena.getId() + ".SpawnLocations." + i + ".Y", loc.getY());
	    	arenaYml.set(arena.getId() + ".SpawnLocations." + i + ".Z", loc.getZ());
	    }
	    arenaYml.set(arena.getId() + ".NumSpawnLocations", i);
	}
	
	public void saveWave(Wave wave){
		
		waveYml.set(wave.getId() + ".Count.Blaze", wave.getCount("Blaze"));
		waveYml.set(wave.getId() + ".Count.Creeper", wave.getCount("Creeper"));
		waveYml.set(wave.getId() + ".Count.EnderMite", wave.getCount("EnderMite"));
		waveYml.set(wave.getId() + ".Count.Evoker", wave.getCount("Evoker"));
		waveYml.set(wave.getId() + ".Count.Ghast", wave.getCount("Ghast"));
		waveYml.set(wave.getId() + ".Count.Gaurdian", wave.getCount("Gaurdian"));
		waveYml.set(wave.getId() + ".Count.Magma_Cube", wave.getCount("Magma_Cube"));
		waveYml.set(wave.getId() + ".Count.Silverfish", wave.getCount("Silverfish"));
		waveYml.set(wave.getId() + ".Count.Skeleton", wave.getCount("Skeleton"));
		waveYml.set(wave.getId() + ".Count.Slime", wave.getCount("Slime"));
		waveYml.set(wave.getId() + ".Count.Stray", wave.getCount("Stray"));
		waveYml.set(wave.getId() + ".Count.Vex", wave.getCount("Vex"));
		waveYml.set(wave.getId() + ".Count.Vindicator", wave.getCount("Vindicator"));
		waveYml.set(wave.getId() + ".Count.Witch", wave.getCount("Witch"));
		waveYml.set(wave.getId() + ".Count.WitherSkeleton", wave.getCount("WitherSkeleton"));
		waveYml.set(wave.getId() + ".Count.Zombie", wave.getCount("Zombie"));
		
		waveYml.set(wave.getId() + ".Level.Blaze", wave.getLevel("Blaze"));
		waveYml.set(wave.getId() + ".Level.Creeper", wave.getLevel("Creeper"));
		waveYml.set(wave.getId() + ".Level.EnderMite", wave.getLevel("EnderMite"));
		waveYml.set(wave.getId() + ".Level.Evoker", wave.getLevel("Evoker"));
		waveYml.set(wave.getId() + ".Level.Ghast", wave.getLevel("Ghast"));
		waveYml.set(wave.getId() + ".Level.Gaurdian", wave.getLevel("Gaurdian"));
		waveYml.set(wave.getId() + ".Level.Magma_Cube", wave.getLevel("Magma_Cube"));
		waveYml.set(wave.getId() + ".Level.Silverfish", wave.getLevel("Silverfish"));
		waveYml.set(wave.getId() + ".Level.Skeleton", wave.getLevel("Skeleton"));
		waveYml.set(wave.getId() + ".Level.Slime", wave.getLevel("Slime"));
		waveYml.set(wave.getId() + ".Level.Stray", wave.getLevel("Stray"));
		waveYml.set(wave.getId() + ".Level.Vex", wave.getLevel("Vex"));
		waveYml.set(wave.getId() + ".Level.Vindicator", wave.getLevel("Vindicator"));
		waveYml.set(wave.getId() + ".Level.Witch", wave.getLevel("Witch"));
		waveYml.set(wave.getId() + ".Level.WitherSkeleton", wave.getLevel("WitherSkeleton"));
		waveYml.set(wave.getId() + ".Level.Zombie", wave.getLevel("Zombie"));
	}
	
}