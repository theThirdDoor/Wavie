package wpve.utilities;

import static wpve.core.Main.CONSOLE;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class StringUtils{

		public static void printInfo(String line){
		    CONSOLE.sendMessage(ChatColor.LIGHT_PURPLE + "[Wavie] " + ChatColor.GREEN + line);
		  }

		  public static void printWarning(String line){
		    CONSOLE.sendMessage(ChatColor.LIGHT_PURPLE + "[Wavie] " + ChatColor.YELLOW + line);
		  }

		  public static void printError(String line){
		    CONSOLE.sendMessage(ChatColor.LIGHT_PURPLE + "[Wavie] " + ChatColor.RED + line);
		  }
		  
		  public static void playerMSG(Player player, String line){
			    player.sendMessage(ChatColor.LIGHT_PURPLE + "[Wavie] " + ChatColor.AQUA + line);
			  }

		  public static String convertToMColors(String line){
		    return line.replaceAll("&", "§");
		  }
}