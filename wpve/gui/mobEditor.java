package wpve.gui;

import static wpve.core.Main.MOB_EDITORS;
import static wpve.core.Main.WAVE_EDITORS;

import java.util.Arrays;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import wpve.core.Wave;

public class mobEditor{
	
	private Wave wave;
	private Player player;
	private Inventory inv;
	private String mobType;
	private HashMap<String, ItemStack> buttons;
	
	public mobEditor(Player player, Wave wave, String mobType){
		
		this.player = player;
		this.wave = wave;
		this.mobType = mobType;
		
		createDisplays();
		createButtons();
		createWindow();
		
	}
	
	private void createDisplays(){
		ItemStack displayObject, mob, mobLevel, mobCount, backButton;
		
		displayObject = new ItemStack(Material.STAINED_GLASS_PANE, 1, (byte) (wave.getId() % 16));
		ItemMeta displayMeta = displayObject.getItemMeta();
		displayMeta.setDisplayName("Wave: " + wave.getId());
		displayMeta.setLore(Arrays.asList(
				"Blaze         : " + wave.getCount("Blaze") + " : " + wave.getLevel("Blaze"),
				"Creeper       : " + wave.getCount("Creeper") + " : " + wave.getLevel("Creeper"),
				"Endermite     : " + wave.getCount("Endermite") + " : " + wave.getLevel("Endermite"),
				"Evoker        : " + wave.getCount("Evoker") + " : " + wave.getLevel("Evoker"),
				"Ghast         : " + wave.getCount("Ghast") + " : " + wave.getLevel("Ghast"),
				"Gaurdian      : " + wave.getCount("Gaurdian") + " : " + wave.getLevel("Gaurdian"),
				"Magma Cube    : " + wave.getCount("Magma_Cube") + " : " + wave.getLevel("Magma_Cube"),
				"Silverfish    : " + wave.getCount("Silverfish") + " : " + wave.getLevel("Silverfish"),
				"Skeleton      : " + wave.getCount("Skeleton") + " : " + wave.getLevel("Skeleton"),
				"Slime         : " + wave.getCount("Slime") + " : " + wave.getLevel("Slime"),
				"Stray         : " + wave.getCount("Stray") + " : " + wave.getLevel("Stray"),
				"Vex           : " + wave.getCount("Vex") + " : " + wave.getLevel("Vex"),
				"Vindicator    : " + wave.getCount("Vindicator") + " : " + wave.getLevel("Vindicator"),
				"Witch         : " + wave.getCount("Witch") + " : " + wave.getLevel("Witch"),
				"WitherSkeleton: " + wave.getCount("WitherSkeleton") + " : " + wave.getLevel("WitherSkeleton"),
				"Zombie        : " + wave.getCount("Zombie") + " : " + wave.getLevel("Zombie")));
		displayObject.setItemMeta(displayMeta);
		
		mob = new ItemStack(Material.EGG, 1);
		ItemMeta mobMeta = mob.getItemMeta();
		mobMeta.setDisplayName(mobType);
		mobMeta.setLore(Arrays.asList(
				"Count: " + wave.getCount(mobType),
				"Level: " + wave.getLevel(mobType)));
		mob.setItemMeta(mobMeta);
		
		mobLevel = new ItemStack(Material.STAINED_GLASS_PANE, wave.getCount(mobType), (byte) 11);
		ItemMeta levelMeta = mobLevel.getItemMeta();
		levelMeta.setLore(Arrays.asList(
				"Level: " + wave.getLevel(mobType),
				"Increase mob level by clicking green wool",
				"Decrease mob level by clicking red wool"));
		mobLevel.setItemMeta(levelMeta);
		
		mobCount = new ItemStack(Material.STAINED_GLASS_PANE, wave.getLevel(mobType), (byte) 14);
		ItemMeta countMeta = mobCount.getItemMeta();
		countMeta.setLore(Arrays.asList(
				"Count: " + wave.getCount(mobType),
				"Increase mob count by clicking green clay",
				"Decrease mob count by clicking red clay"));
		mobLevel.setItemMeta(levelMeta);
		
		backButton = new ItemStack(Material.REDSTONE_BLOCK, 1);
		ItemMeta backMeta = backButton.getItemMeta();
		backMeta.setDisplayName(ChatColor.RED + "BACK!");
		backButton.setItemMeta(backMeta);
		
		buttons.put("displayObject", displayObject);
		buttons.put("mob", mob);
		buttons.put("mobLevel", mobLevel);
		buttons.put("mobCount", mobCount);
		buttons.put("backButton", backButton);
	}
	
	private void createButtons(){
		ItemStack LU50, LU10, LU5, LU1, LD1, LD5, LD10, LD50, CU50, CU10, CU5, CU1, CD1, CD5, CD10, CD50;
		LU50 = new ItemStack(Material.WOOL, 50, (byte) 5);
		LU10 = new ItemStack(Material.WOOL, 10, (byte) 5);
		LU5 = new ItemStack(Material.WOOL, 5, (byte) 5);
		LU1 = new ItemStack(Material.WOOL, 1, (byte) 5);
		LD1 = new ItemStack(Material.WOOL, 1, (byte) 14);
		LD5 = new ItemStack(Material.WOOL, 5, (byte) 14);
		LD10 = new ItemStack(Material.WOOL, 10, (byte) 14);
		LD50 = new ItemStack(Material.WOOL, 50, (byte) 14);
		CU50 = new ItemStack(Material.STAINED_CLAY, 50, (byte) 13);
		CU10 = new ItemStack(Material.STAINED_CLAY, 10, (byte) 13);
		CU5 = new ItemStack(Material.STAINED_CLAY, 5, (byte) 13);
		CU1 = new ItemStack(Material.STAINED_CLAY, 1, (byte) 13);
		CD1 = new ItemStack(Material.STAINED_CLAY, 1, (byte) 14);
		CD5 = new ItemStack(Material.STAINED_CLAY, 5, (byte) 14);
		CD10 = new ItemStack(Material.STAINED_CLAY, 10, (byte) 14);
		CD50 = new ItemStack(Material.STAINED_CLAY, 50, (byte) 14);
		
		buttons.put("LU50", LU50);
		buttons.put("LU10", LU10);
		buttons.put("LU5", LU5);
		buttons.put("LU1", LU1);
		buttons.put("LD50", LD50);
		buttons.put("LD10", LD10);
		buttons.put("LD5", LD5);
		buttons.put("LD1", LD1);
		buttons.put("CU50", CU50);
		buttons.put("CU10", CU10);
		buttons.put("CU5", CU5);
		buttons.put("CU1", CU1);
		buttons.put("CD50", CD50);
		buttons.put("CD10", CD10);
		buttons.put("CD5", CD5);
		buttons.put("CD1", CD1);
	}
	
	private void createWindow(){
		inv = Bukkit.createInventory(null, 36);
		inv.setItem(4, buttons.get("displayObject"));
		inv.setItem(3, buttons.get("mob"));
		inv.setItem(5, buttons.get("mob"));
		inv.setItem(13, buttons.get("mobLevel"));
		inv.setItem(22, buttons.get("mobCount"));
		inv.setItem(31, buttons.get("back"));
		
		inv.setItem(9, buttons.get("LD50"));
		inv.setItem(10, buttons.get("LD10"));
		inv.setItem(11, buttons.get("LD5"));
		inv.setItem(12, buttons.get("LD1"));
		
		inv.setItem(14, buttons.get("LU1"));
		inv.setItem(15, buttons.get("LU5"));
		inv.setItem(16, buttons.get("LU10"));
		inv.setItem(17, buttons.get("LU50"));
		
		inv.setItem(18, buttons.get("CU50"));
		inv.setItem(19, buttons.get("CU10"));
		inv.setItem(20, buttons.get("CU5"));
		inv.setItem(21, buttons.get("CU1"));
		
		inv.setItem(23, buttons.get("CU1"));
		inv.setItem(24, buttons.get("CU5"));
		inv.setItem(25, buttons.get("CU10"));
		inv.setItem(26, buttons.get("CU50"));
		
		player.openInventory(inv);
	}
	
	public void handleClickEvent(InventoryClickEvent event) {
		if (inv.getSize() > event.getSlot() && event.getSlot() >= 0) {
			ItemStack item = inv.getItem(event.getSlot());
			if (item != null) {
				if(goBack(event)){
					player.closeInventory();							
					WAVE_EDITORS.put(player, new waveEditor(player, wave));
				} else {
					int count = wave.getCount(mobType);
					int level = wave.getLevel(mobType);
					
					if (9 == event.getSlot()) {
						wave.setCount(mobType, count - 50);
					}
					if (10 == event.getSlot()) {
						wave.setCount(mobType, count - 10);
					}
					if (11 == event.getSlot()) {
						wave.setCount(mobType, count - 5);
					}
					if (12 == event.getSlot()) {
						wave.setCount(mobType, count - 1);
					}
					if (14 == event.getSlot()) {
						wave.setCount(mobType, count + 1);
					}
					if (15 == event.getSlot()) {
						wave.setCount(mobType, count + 5);
					}
					if (16 == event.getSlot()) {
						wave.setCount(mobType, count + 10);
					}
					if (17 == event.getSlot()) {
						wave.setCount(mobType, count + 50);
					}
					
					if (18 == event.getSlot()) {
						wave.setLeveL(mobType, level - 50);
					}
					if (19 == event.getSlot()) {
						wave.setLeveL(mobType, level - 10);
					}
					if (20 == event.getSlot()) {
						wave.setLeveL(mobType, level - 5);
					}
					if (21 == event.getSlot()) {
						wave.setLeveL(mobType, level - 1);
					}
					if (23 == event.getSlot()) {
						wave.setLeveL(mobType, level + 1);
					}
					if (24 == event.getSlot()) {
						wave.setLeveL(mobType, level + 5);
					}
					if (25 == event.getSlot()) {
						wave.setLeveL(mobType, level + 10);
					}
					if (26 == event.getSlot()) {
						wave.setLeveL(mobType, level + 50);
					}
					
					
					player.closeInventory();					
					MOB_EDITORS.put(player, new mobEditor(player, wave, mobType));
				}
			}
		}
		
	}
	
	private boolean goBack(InventoryClickEvent event){
		if(event.getSlot() == 3){
			return true;
		}
		if(event.getSlot() == 4){
			return true;
		}
		if(event.getSlot() == 5){
			return true;
		}
		if(event.getSlot() == 31){
			return true;
		}
		return false;
	}
}