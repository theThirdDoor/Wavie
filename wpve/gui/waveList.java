package wpve.gui;

import static wpve.core.Main.WAVE_LISTS;
import static wpve.core.Main.WAVE_EDITORS;
import static wpve.core.Main.WAVES;

import java.util.Arrays;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import wpve.core.Main;
import wpve.core.Wave;

public class waveList{
	private Inventory inv;
	private Player player;
	private HashMap<Integer, Wave> windowWaves = new HashMap<Integer, Wave>();
	private ItemStack page1;
	private ItemStack page2;
	private ItemStack page3;
	private ItemStack page4;
	private ItemStack page5;
	private ItemStack page6;
	private ItemStack page7;
	private ItemStack page8;
	private ItemStack page9;
	private int pageNumber;
	
	public waveList(Player player, int pageIn){
		this.player = player;
		pageNumber = pageIn;
		
		createPage1();
		createPage2();
		createPage3();
		createPage4();
		createPage5();
		createPage6();
		createPage7();
		createPage8();
		createPage9();
		window();
	}

	private void createPage1(){
		page1 = new ItemStack(Material.WOOL, 1, (byte) 5);
		ItemMeta meta = page1.getItemMeta();
		meta.setDisplayName("page1");
		page1.setItemMeta(meta);
	}
	
	private void createPage2(){
		page2 = new ItemStack(Material.WOOL, 1, (byte) 6);
		ItemMeta meta = page2.getItemMeta();
		meta.setDisplayName("page2");
		page2.setItemMeta(meta);
	}
	
	private void createPage3(){
		page3 = new ItemStack(Material.WOOL, 1, (byte) 5);
		ItemMeta meta = page3.getItemMeta();
		meta.setDisplayName("page3");
		page3.setItemMeta(meta);
	}
	
	private void createPage4(){
		page4 = new ItemStack(Material.WOOL, 1, (byte) 6);
		ItemMeta meta = page4.getItemMeta();
		meta.setDisplayName("page4");
		page4.setItemMeta(meta);
	}
	
	private void createPage5(){
		page5 = new ItemStack(Material.WOOL, 1, (byte) 5);
		ItemMeta meta = page5.getItemMeta();
		meta.setDisplayName("page5");
		page5.setItemMeta(meta);
	}
	
	private void createPage6(){
		page6 = new ItemStack(Material.WOOL, 1, (byte) 6);
		ItemMeta meta = page6.getItemMeta();
		meta.setDisplayName("page6");
		page6.setItemMeta(meta);
	}
	
	private void createPage7(){
		page7 = new ItemStack(Material.WOOL, 1, (byte) 5);
		ItemMeta meta = page7.getItemMeta();
		meta.setDisplayName("page7");
		page7.setItemMeta(meta);
	}
	
	private void createPage8(){
		page8 = new ItemStack(Material.WOOL, 1, (byte) 6);
		ItemMeta meta = page8.getItemMeta();
		meta.setDisplayName("page8");
		page8.setItemMeta(meta);
	}
	
	private void createPage9(){
		page9 = new ItemStack(Material.WOOL, 1, (byte) 5);
		ItemMeta meta = page9.getItemMeta();
		meta.setDisplayName("page9");
		page9.setItemMeta(meta);
	}

	private void window() {
		
		inv = Bukkit.createInventory(null, 54);
		
		inv.setItem(0, page1);
		inv.setItem(1, page2);
		inv.setItem(2, page3);
		inv.setItem(3, page4);
		inv.setItem(4, page5);
		inv.setItem(5, page6);
		inv.setItem(6, page7);
		inv.setItem(7, page8);
		inv.setItem(8, page9);
		
		int slot = 9;
		
		int waveIconsPlaced = 0;
		
		for (int i = 0; i < 405; i++){
			if((waveIconsPlaced >= (45 * (pageNumber - 1))) && (waveIconsPlaced < (45 * pageNumber))){
				ItemStack waveIcon;
				
				if(WAVES.get(i) == null){
					waveIcon = new ItemStack(Material.THIN_GLASS, 1);
					ItemMeta waveData = waveIcon.getItemMeta();
					waveData.setLore(Arrays.asList("Error on load"));						
					waveIcon.setItemMeta(waveData);
					
				} else {
					Wave w = WAVES.get(i);
					waveIcon = new ItemStack(Material.STAINED_GLASS_PANE, 1, (byte) (w.getId() % 16));
					ItemMeta waveData = waveIcon.getItemMeta();
					waveData.setLore(Arrays.asList("Mob type, count, lvl (relative)", 
							
							"Blaze         : " + w.getCount("Blaze") + " : " + w.getLevel("Blaze"),
							"Creeper       : " + w.getCount("Creeper") + " : " + w.getLevel("Creeper"),
							"Endermite     : " + w.getCount("Endermite") + " : " + w.getLevel("Endermite"),
							"Evoker        : " + w.getCount("Evoker") + " : " + w.getLevel("Evoker"),
							"Ghast         : " + w.getCount("Ghast") + " : " + w.getLevel("Ghast"),
							"Gaurdian      : " + w.getCount("Gaurdian") + " : " + w.getLevel("Gaurdian"),
							"Magma Cube    : " + w.getCount("Magma_Cube") + " : " + w.getLevel("Magma_Cube"),
							"Silverfish    : " + w.getCount("Silverfish") + " : " + w.getLevel("Silverfish"),
							"Skeleton      : " + w.getCount("Skeleton") + " : " + w.getLevel("Skeleton"),
							"Slime         : " + w.getCount("Slime") + " : " + w.getLevel("Slime"),
							"Stray         : " + w.getCount("Stray") + " : " + w.getLevel("Stray"),
							"Vex           : " + w.getCount("Vex") + " : " + w.getLevel("Vex"),
							"Vindicator    : " + w.getCount("Vindicator") + " : " + w.getLevel("Vindicator"),
							"Witch         : " + w.getCount("Witch") + " : " + w.getLevel("Witch"),
							"WitherSkeleton: " + w.getCount("WitherSkeleton") + " : " + w.getLevel("WitherSkeleton"),
							"Zombie        : " + w.getCount("Zombie") + " : " + w.getLevel("Zombie")));
					waveIcon.setItemMeta(waveData);
					windowWaves.put(slot, w);
				}
				inv.setItem(slot, waveIcon);
				
				
				slot ++;
			}
			waveIconsPlaced++;
		}
		player.openInventory(inv);                   
	}
	
	public void HandleClickEvent(InventoryClickEvent event) {
		if (inv.getSize() > event.getSlot() && event.getSlot() >= 0) {
			ItemStack item = inv.getItem(event.getSlot());
			if (item != null) {
				player.closeInventory();
				if (9 > event.getSlot()) {
					
					int i = (event.getSlot() + 1);
					player.closeInventory();
					event.setCancelled(true);
					waveList win = new waveList(player, i);
					WAVE_LISTS.put(player, win);
					return;
				}
				Wave wave;
				int id = event.getSlot() - 9 + (45 * (pageNumber - 1));				
							
				if (event.getCurrentItem().getType() == Material.THIN_GLASS){
					Main.fileman.createWave(id);
					wave = WAVES.get(id);
					Main.fileman.saveWave(wave);
				}
				wave = WAVES.get(id);
				WAVE_EDITORS.put(player, new waveEditor(player, wave));
			}
		}
	}
	
}