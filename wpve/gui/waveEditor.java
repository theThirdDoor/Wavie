package wpve.gui;

import static wpve.core.Main.MOB_EDITORS;
import static wpve.core.Main.WAVE_LISTS;

import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import wpve.core.Wave;
import wpve.utilities.StringUtils;

public class waveEditor {
	
	private Player player;
	private Inventory inv;
	private Wave wave;
	private ItemStack displayObject;
	private ItemStack backButton;
	
	private ItemStack Blaze;
	private ItemStack Creeper;
	private ItemStack Endermite;
	private ItemStack Evoker;
	private ItemStack Ghast;
	private ItemStack Gaurdian;
	private ItemStack Magma_Cube;
	private ItemStack Silverfish;
	private ItemStack Skeleton;
	private ItemStack Slime;
	private ItemStack Stray;
	private ItemStack Vex;
	private ItemStack Vindicator;
	private ItemStack Witch;
	private ItemStack WitherSkeleton;
	private ItemStack Zombie;
	
	public waveEditor(Player player, Wave wave){
		
		if(wave == null){
			StringUtils.playerMSG(player, "null wave, fix bug");
			return;
		}
		
		this.wave = wave;
		
		createDisplayObject();
		createBackButton();
		createMobButtons();

		inv = Bukkit.createInventory(player, 54);
		inv.setItem(4, displayObject);
		inv.setItem(49, backButton);
		inv.setItem(10, Blaze);
		inv.setItem(19, Creeper);
		inv.setItem(28, Endermite);
		inv.setItem(37, Evoker);
		inv.setItem(12, Ghast);
		inv.setItem(21, Gaurdian);
		inv.setItem(30, Magma_Cube);
		inv.setItem(39, Silverfish);
		inv.setItem(14, Skeleton);
		inv.setItem(23, Slime);
		inv.setItem(32, Stray);
		inv.setItem(41, Vex);
		inv.setItem(16, Vindicator);
		inv.setItem(25, Witch);
		inv.setItem(34, WitherSkeleton);
		inv.setItem(43, Zombie);
		
		player.openInventory(inv);

	}
	
	private void createDisplayObject(){
		displayObject = new ItemStack(Material.STAINED_GLASS_PANE, 1, (byte) (wave.getId() % 16));
		ItemMeta displayMeta = displayObject.getItemMeta();
		displayMeta.setDisplayName("Wave: " + wave.getId());
		displayMeta.setLore(Arrays.asList(
				"Blaze         : " + wave.getCount("Blaze") + " : " + wave.getLevel("Blaze"),
				"Creeper       : " + wave.getCount("Creeper") + " : " + wave.getLevel("Creeper"),
				"Endermite     : " + wave.getCount("Endermite") + " : " + wave.getLevel("Endermite"),
				"Evoker        : " + wave.getCount("Evoker") + " : " + wave.getLevel("Evoker"),
				"Ghast         : " + wave.getCount("Ghast") + " : " + wave.getLevel("Ghast"),
				"Gaurdian      : " + wave.getCount("Gaurdian") + " : " + wave.getLevel("Gaurdian"),
				"Magma Cube    : " + wave.getCount("Magma_Cube") + " : " + wave.getLevel("Magma_Cube"),
				"Silverfish    : " + wave.getCount("Silverfish") + " : " + wave.getLevel("Silverfish"),
				"Skeleton      : " + wave.getCount("Skeleton") + " : " + wave.getLevel("Skeleton"),
				"Slime         : " + wave.getCount("Slime") + " : " + wave.getLevel("Slime"),
				"Stray         : " + wave.getCount("Stray") + " : " + wave.getLevel("Stray"),
				"Vex           : " + wave.getCount("Vex") + " : " + wave.getLevel("Vex"),
				"Vindicator    : " + wave.getCount("Vindicator") + " : " + wave.getLevel("Vindicator"),
				"Witch         : " + wave.getCount("Witch") + " : " + wave.getLevel("Witch"),
				"WitherSkeleton: " + wave.getCount("WitherSkeleton") + " : " + wave.getLevel("WitherSkeleton"),
				"Zombie        : " + wave.getCount("Zombie") + " : " + wave.getLevel("Zombie")));
		displayObject.setItemMeta(displayMeta);
	}
	
	private void createBackButton(){
		backButton = new ItemStack(Material.REDSTONE_BLOCK, 1);
		ItemMeta backMeta = backButton.getItemMeta();
		backMeta.setDisplayName(ChatColor.RED + "BACK!");
		backButton.setItemMeta(backMeta);
	}
	
	private void createMobButtons(){

		Blaze = new ItemStack(Material.WOOL, 1, (byte) 0);
		ItemMeta BlazeMeta = Blaze.getItemMeta();
		BlazeMeta.setDisplayName("Blaze");
		BlazeMeta.setLore(Arrays.asList(
				"Count: " + wave.getCount("Blaze"),	
				"Level: " + wave.getLevel("Blaze")));
		Blaze.setItemMeta(BlazeMeta);
		
		Creeper = new ItemStack(Material.WOOL, 1, (byte) 1);
		ItemMeta CreeperMeta = Creeper.getItemMeta();
		CreeperMeta.setDisplayName("Creeper");
		CreeperMeta.setLore(Arrays.asList(
				"Count: " + wave.getCount("Creeper"),
				"Level: " + wave.getLevel("Creeper")));	
		Creeper.setItemMeta(CreeperMeta);
		
		Endermite = new ItemStack(Material.WOOL, 1, (byte) 2);	
		ItemMeta EnderMiteMeta = Endermite.getItemMeta();
		EnderMiteMeta.setDisplayName("Endermite");
		EnderMiteMeta.setLore(Arrays.asList(
				"Count: " + wave.getCount("Endermite"),	
				"Level: " + wave.getLevel("Endermite")));	
		Endermite.setItemMeta(EnderMiteMeta);
		
		Evoker = new ItemStack(Material.WOOL, 1, (byte) 3);	
		ItemMeta EvokerMeta = Evoker.getItemMeta();	
		EvokerMeta.setDisplayName("Evoker");	
		EvokerMeta.setLore(Arrays.asList(	
				"Count: " + wave.getCount("Evoker"),
				"Level: " + wave.getLevel("Evoker")));	
		Evoker.setItemMeta(EvokerMeta);
		
		Ghast = new ItemStack(Material.WOOL, 1, (byte) 4);
		ItemMeta GhastMeta = Ghast.getItemMeta();	
		GhastMeta.setDisplayName("Ghast");
		GhastMeta.setLore(Arrays.asList(
				"Count: " + wave.getCount("Ghast"),	
				"Level: " + wave.getLevel("Ghast")));
		Ghast.setItemMeta(GhastMeta);
		
		Gaurdian = new ItemStack(Material.WOOL, 1, (byte) 5);
		ItemMeta GaurdianMeta = Gaurdian.getItemMeta();	
		GaurdianMeta.setDisplayName("Gaurdian");	
		GaurdianMeta.setLore(Arrays.asList(	
				"Count: " + wave.getCount("Gaurdian"),
				"Level: " + wave.getLevel("Gaurdian")));
		Gaurdian.setItemMeta(GaurdianMeta);
		
		Magma_Cube = new ItemStack(Material.WOOL, 1, (byte) 6);	
		ItemMeta Magma_CubeMeta = Magma_Cube.getItemMeta();	
		Magma_CubeMeta.setDisplayName("Magma_Cube");	
		Magma_CubeMeta.setLore(Arrays.asList(
				"Count: " + wave.getCount("Magma_Cube"),
				"Level: " + wave.getLevel("Magma_Cube")));	
		Magma_Cube.setItemMeta(Magma_CubeMeta);
		
		Silverfish = new ItemStack(Material.WOOL, 1, (byte) 7);	
		ItemMeta SilverfishMeta = Silverfish.getItemMeta();
		SilverfishMeta.setDisplayName("Silverfish");
		SilverfishMeta.setLore(Arrays.asList(
				"Count: " + wave.getCount("Silverfish"),
				"Level: " + wave.getLevel("Silverfish")));
		Silverfish.setItemMeta(SilverfishMeta);
		
		Skeleton = new ItemStack(Material.WOOL, 1, (byte) 8);
		ItemMeta SkeletonMeta = Skeleton.getItemMeta();	
		SkeletonMeta.setDisplayName("Skeleton");	
		SkeletonMeta.setLore(Arrays.asList(
				"Count: " + wave.getCount("Skeleton"),
				"Level: " + wave.getLevel("Skeleton")));
		Skeleton.setItemMeta(SkeletonMeta);
		
		Slime = new ItemStack(Material.WOOL, 1, (byte) 9);
		ItemMeta SlimeMeta = Slime.getItemMeta();
		SlimeMeta.setDisplayName("Slime");
		SlimeMeta.setLore(Arrays.asList(
				"Count: " + wave.getCount("Slime"),
				"Level: " + wave.getLevel("Slime")));
		Slime.setItemMeta(SlimeMeta);
		
		Stray = new ItemStack(Material.WOOL, 1, (byte) 10);
		ItemMeta StrayMeta = Stray.getItemMeta();
		StrayMeta.setDisplayName("Stray");
		StrayMeta.setLore(Arrays.asList(
				"Count: " + wave.getCount("Stray"),	
				"Level: " + wave.getLevel("Stray")));
		Stray.setItemMeta(StrayMeta);
		
		Vex = new ItemStack(Material.WOOL, 1, (byte) 11);
		ItemMeta VexMeta = Vex.getItemMeta();
		VexMeta.setDisplayName("Vex");	
		VexMeta.setLore(Arrays.asList(
				"Count: " + wave.getCount("Vex"),
				"Level: " + wave.getLevel("Vex")));
		Vex.setItemMeta(VexMeta);
		
		Vindicator = new ItemStack(Material.WOOL, 1, (byte) 12);	
		ItemMeta VindicatorMeta = Vindicator.getItemMeta();
		VindicatorMeta.setDisplayName("Vindicator");
		VindicatorMeta.setLore(Arrays.asList(
				"Count: " + wave.getCount("Vindicator"),
				"Level: " + wave.getLevel("Vindicator")));
		Vindicator.setItemMeta(VindicatorMeta);
		
		Witch = new ItemStack(Material.WOOL, 1, (byte) 13);
		ItemMeta WitchMeta = Witch.getItemMeta();
		WitchMeta.setDisplayName("Witch");
		WitchMeta.setLore(Arrays.asList(
				"Count: " + wave.getCount("Witch"),
				"Level: " + wave.getLevel("Witch")));
		Witch.setItemMeta(WitchMeta);
		
		WitherSkeleton = new ItemStack(Material.WOOL, 1, (byte) 14);
		ItemMeta WitherSkeletonMeta = WitherSkeleton.getItemMeta();
		WitherSkeletonMeta.setDisplayName("WitherSkeleton");
		WitherSkeletonMeta.setLore(Arrays.asList(
				"Count: " + wave.getCount("WitherSkeleton"),
				"Level: " + wave.getLevel("WitherSkeleton")));
		WitherSkeleton.setItemMeta(WitherSkeletonMeta);
		
		Zombie = new ItemStack(Material.WOOL, 1, (byte) 15);
		ItemMeta ZombieMeta = Zombie.getItemMeta();
		ZombieMeta.setDisplayName("Zombie");
		ZombieMeta.setLore(Arrays.asList(
				"Count: " + wave.getCount("Zombie"),
				"Level: " + wave.getLevel("Zombie")));
		Zombie.setItemMeta(ZombieMeta);
	}

	public void handleClickEvent(InventoryClickEvent event) {
		if (inv.getSize() > event.getSlot() && event.getSlot() >= 0) {
			ItemStack item = inv.getItem(event.getSlot());
			if (item != null) {
				if (49 == event.getSlot() || event.getSlot() == 4) {
					
					waveList win = new waveList(player, 1);

					WAVE_LISTS.put(player, win);
					
				} else {
					String mobType;
					if(event.getSlot() == 10){
						mobType = "Blaze";
					}
					if(event.getSlot() == 19){
						mobType = "Creeper";
					}
					if(event.getSlot() == 28){
						mobType = "Endermite";
					}
					if(event.getSlot() == 37){
						mobType = "Evoker";
					}
					if(event.getSlot() == 12){
						mobType = "Ghast";
					}
					if(event.getSlot() == 21){
						mobType = "Gaurdian";
					}
					if(event.getSlot() == 30){
						mobType = "Magma_Cube";
					}
					if(event.getSlot() == 39){
						mobType = "Silverfish";
					}
					if(event.getSlot() == 14){
						mobType = "Skeleton";
					}
					if(event.getSlot() == 23){
						mobType = "Slime";
					}
					if(event.getSlot() == 32){
						mobType = "Stray";
					}
					if(event.getSlot() == 41){
						mobType = "Vex";
					}
					if(event.getSlot() == 16){
						mobType = "Vindicator";
					}
					if(event.getSlot() == 25){
						mobType = "Witch";
					}
					if(event.getSlot() == 34){
						mobType = "WitherSkeleton";
					}
					else {
						mobType = "Zombie";
					}			
					
					player.closeInventory();
					
					event.setCancelled(true);
					MOB_EDITORS.put(player,  new mobEditor(player, wave, mobType));
				}
				
			}
		}
	}
}