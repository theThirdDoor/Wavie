package wpve.core;

import java.util.HashMap;

public class Wave {
	
	private int ID;
	private String name;
	
	private HashMap<String, Integer> Count;
	private HashMap<String, Integer> Level;
	
	
	
	public Wave() {
	}

	public int getId() {
		return ID;
	}

	public void setId(int id) {
		this.ID = id;
	}	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public int getCount(String mobType){
		return Count.get(mobType);
	}
	
	public void setCount(HashMap<String, Integer> count){
		this.Count = count;
	}
	
	public void setCount(String mobType, int count){
		this.Count.put(mobType, count);
	}
	
	public int getLevel(String mobType){
		return Level.get(mobType);
	}
	
	public void setLevel(HashMap<String, Integer> level){
		this.Level = level;
	}
	
	public void setLeveL(String mobType, int Level){
		this.Level.put(mobType, Level);
	}
}