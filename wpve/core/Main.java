package wpve.core;

import static wpve.utilities.StringUtils.*;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import wpve.gui.waveEditor;
import wpve.gui.waveList;
import wpve.gui.mobEditor;
import wpve.handlers.ClickHandler;
import wpve.handlers.CommandListener;
import wpve.handlers.SpawnListener;
import wpve.utilities.FileMan;
import wpve.utilities.StringUtils;

import org.bukkit.Bukkit;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin implements Listener {
	
	//Data holders
	public static ArrayList<Arena> ArenaList = new ArrayList<Arena>();
	public static HashMap<String, Arena> ARENAS = new HashMap<String, Arena>();
	public static ArrayList<Wave> WaveList = new ArrayList<Wave>();
	public static HashMap<Integer, Wave> WAVES = new HashMap<Integer, Wave>();
	
	//On click handlers
	public static HashMap<Player, waveList> WAVE_LISTS = new HashMap<Player, waveList>();
	public static HashMap<Player, waveEditor> WAVE_EDITORS = new HashMap<Player, waveEditor>();
	public static HashMap<Player, mobEditor> MOB_EDITORS = new HashMap<Player, mobEditor>();

	
	public static ConsoleCommandSender CONSOLE;
	public File arenaFile;
	public StringUtils stringUtils = new StringUtils();
	public static FileMan fileman;
	public static YamlConfiguration arenaYml;
	
	@Override
	public void onEnable() {
		
		getCommand("wavie").setExecutor(new CommandListener(this));
		CONSOLE = Bukkit.getServer().getConsoleSender();
		getServer().getPluginManager().registerEvents(new SpawnListener(this), this);
		getServer().getPluginManager().registerEvents(new ClickHandler(), this);
		
		Main.fileman = new FileMan();
		fileman.loadFiles(this.getDataFolder());
		
		printInfo("has been enabled!");
	}

	@Override
	public void onDisable() {
		printWarning("shutting down");
		fileman.saveFile();
		
	}
	
}
	