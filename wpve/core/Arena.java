package wpve.core;

import java.util.ArrayList;

import org.bukkit.Location;
import org.bukkit.entity.Player;

public class Arena {
	
	private String ID;
	private String name;
	private Location loc;
	private ArrayList<Location> spawnLocations;
	
	public Arena() {
		
	}
	
	public String getId() {
	    return ID;
	}

	public void setId(String id) {
	    this.ID = id;
	}
	
	public String getName() {
	    return name;
	}

	public void setName(String name) {
		  this.name = name;
	}
	
	public Location getLocation() {
	    return loc;
	}

	public void setLocation(Location loc) {
	    this.loc = loc;
	}
	
	public ArrayList<Location> getSpawnLocations() {
	    return spawnLocations;
	}

	public void setSpawnLocations(ArrayList<Location> spawnLocations) {
	    this.spawnLocations = spawnLocations;
	}
	
	public void teleport(Player player){
		player.teleport(loc.clone().add(0, 1, 0));
	}
}