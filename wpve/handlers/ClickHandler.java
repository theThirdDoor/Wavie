package wpve.handlers;

import static wpve.core.Main.WAVE_LISTS;
import static wpve.core.Main.WAVE_EDITORS;
import static wpve.core.Main.MOB_EDITORS;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;

import wpve.gui.mobEditor;
import wpve.gui.waveEditor;
import wpve.gui.waveList;

public class ClickHandler implements Listener {

  @EventHandler
  	public void OnPlayerClickInventory(InventoryClickEvent e) {
	    if (!(e.getWhoClicked() instanceof Player)) {
	    	return;
	    }
	    if (WAVE_LISTS.containsKey(e.getWhoClicked())){
		      waveList win = WAVE_LISTS.get(e.getWhoClicked());
		      win.HandleClickEvent(e);
		      
	    		e.setCancelled(true);
	    }
	    if (WAVE_EDITORS.containsKey(e.getWhoClicked())) {
		      waveEditor win = WAVE_EDITORS.get(e.getWhoClicked());
		      win.handleClickEvent(e);
		      
	    		e.setCancelled(true);
	    }
    	if (MOB_EDITORS.containsKey(e.getWhoClicked())){
    		mobEditor win = MOB_EDITORS.get(e.getWhoClicked());
    		win.handleClickEvent(e);
    		
    		e.setCancelled(true);
    	}
  }

  @EventHandler
  public void OnWindowClose(InventoryCloseEvent e) {
	  if (!(e.getPlayer() instanceof Player)) {
		  return;
	  }
	  if (WAVE_LISTS.containsKey(e.getPlayer())) {
		  WAVE_LISTS.remove(e.getPlayer());
	  }
	  if (WAVE_EDITORS.containsKey(e.getPlayer())) {
		  WAVE_EDITORS.remove(e.getPlayer());
	  }
	  if (MOB_EDITORS.containsKey(e.getPlayer())) {
		  MOB_EDITORS.containsKey(e.getPlayer());
	  }
  	}
}
