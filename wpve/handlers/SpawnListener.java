package wpve.handlers;

import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;

import wpve.core.Arena;
import wpve.core.Main;

public class SpawnListener implements Listener {

	public SpawnListener(Main plugin) {
	}
	
	@EventHandler
	public void mainSpawn(CreatureSpawnEvent event){
		
		Location loc = event.getLocation();
		for(Arena arena : Main.ArenaList){
			
			if(mainZoneChecker(loc, arena)){
				event.setCancelled(true);
				return;
			}
		}
	}
	
	Boolean mainZoneChecker(Location loc, Arena arena){
		if(loc.getWorld() == arena.getLocation().getWorld()){
			if(loc.getX() > (arena.getLocation().getX() - 100) && loc.getX() < (arena.getLocation().getX() + 99)){
				if(loc.getBlockZ() > (arena.getLocation().getBlockZ() - 100) && loc.getBlockZ() < (arena.getLocation().getBlockZ() +99)){
					return true;
				}
			}
		}
		return false;
	}
}