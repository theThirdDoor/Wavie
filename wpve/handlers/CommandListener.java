package wpve.handlers;

import static wpve.core.Main.WAVE_LISTS;

import java.util.ArrayList;
import java.util.HashSet;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import wpve.core.Arena;
import wpve.core.Main;
import wpve.gui.waveList;
import wpve.utilities.StringUtils;
import wpve.handlers.SpawnListener;

public class CommandListener implements CommandExecutor {
	
	private SpawnListener spawnListener;
	
	public CommandListener(Main plugin) {
		this.spawnListener = new SpawnListener(plugin);
	  }
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args){
		
		if(sender instanceof ConsoleCommandSender){
			StringUtils.printInfo("active");
			return true;
	    }
	
		if(sender instanceof Player) {
			Player player = (Player) sender;
			
			if(args[0].equalsIgnoreCase("test")){
				//TODO
				if(Main.ARENAS.get(args[1]) != null){
					Arena arena = Main.ARENAS.get(args[1]);
					arena.teleport(player);
					return true;
				}
			}
			
			//if(player.hasPermission("Wavie.admin")){
				if(args.length == 0){
					StringUtils.playerMSG(player, "");
					
					return true;
				}
				
				if(args[0].equalsIgnoreCase("create")){
					create(player);
					return true;
			    }
				
				if(args[0].equalsIgnoreCase("waves")){
					player.sendMessage("Opening wave editor!");
					waveList win = new waveList(player, 1);
					WAVE_LISTS.put(player, win);
				}
				
				if(args[0].equalsIgnoreCase("edit")){					
					
					if(Main.ARENAS.get(args[1]) != null){
						Arena arena = Main.ARENAS.get(args[1]);
						Location pLoc = player.getLocation();
						
						//if in arena
						if(spawnListener.mainZoneChecker(pLoc, arena)){

							if(args[2].equalsIgnoreCase("spawnZones")){
								
								if(args[3].equalsIgnoreCase("add")){
									editSZAdd(arena, player);
								}
								
								else if(args[3].equalsIgnoreCase("remove")){
									editSZRemove(arena, player);
								}
								
								else if(args[3].equalsIgnoreCase("show")){
									//TODO
									//GLOWSTONE
								}
								
								else if(args[3].equalsIgnoreCase("hide")){
									//TODO
								}
								
								else if(args[3].equalsIgnoreCase("count")){
									//TODO
								}
							}
							
							if(args[2].equalsIgnoreCase("waves")){
								//TODO
								//GUI
							}
						}
					}
				}
			//}
		}
	return true;
	}
	
	@SuppressWarnings("deprecation")
	void create(Player player){
		
		Block block = player.getTargetBlock((HashSet<Byte>) null, 10);
		
		String ID = checkSigal(player, block);
		if(ID == "NoMatch"){
			StringUtils.playerMSG(player, "please insert matching ID blocks!");
			return;
		}
		if(ID == "AIR"){
			StringUtils.playerMSG(player, "please insert ID blocks!");
			return;
		}
		
		Arena arena = Main.ARENAS.get(ID);
		
		if (arena != null) {
			StringUtils.playerMSG(player, "An arena with this ID already exists!");
			return;
		}
		
		Main.fileman.createArena(ID, block.getLocation());
		
		StringUtils.playerMSG(player, ID + " arena created!");
	}
	
	String checkSigal(Player player, Block block){
		
		String retVal = "NULL";
		
		if(block.getType() == Material.LAPIS_BLOCK){
			Location loc = block.getLocation();
			
			boolean B1 = loc.clone().add(-1, 0, 0).getBlock().getType() == Material.REDSTONE_BLOCK;
			boolean B2 = loc.clone().add(-1, 0, -1).getBlock().getType() == Material.REDSTONE_BLOCK;
			boolean B3 = loc.clone().add(0, 0, -1).getBlock().getType() == Material.REDSTONE_BLOCK;
			
			if(B1 && B2 && B3){
				boolean C1 = loc.clone().add(2, 0, -1).getBlock().getType() == Material.COBBLESTONE;
				boolean C2 = loc.clone().add(2, 0, 0).getBlock().getType() == Material.COBBLESTONE;
				boolean C3 = loc.clone().add(1, 0, 1).getBlock().getType() == Material.COBBLESTONE;
				boolean C4 = loc.clone().add(0, 0, 2).getBlock().getType() == Material.COBBLESTONE;
				boolean C5 = loc.clone().add(-1, 0, 2).getBlock().getType() == Material.COBBLESTONE;
				boolean C6 = loc.clone().add(-2, 0, 1).getBlock().getType() == Material.COBBLESTONE;
				boolean C7 = loc.clone().add(-3, 0, 0).getBlock().getType() == Material.COBBLESTONE;
				boolean C8 = loc.clone().add(-3, 0, -1).getBlock().getType() == Material.COBBLESTONE;
				boolean C9 = loc.clone().add(-2, 0, -2).getBlock().getType() == Material.COBBLESTONE;
				boolean C10 = loc.clone().add(-1, 0, -3).getBlock().getType() == Material.COBBLESTONE;
				boolean C11 = loc.clone().add(0, 0, -3).getBlock().getType() == Material.COBBLESTONE;
				boolean C12 = loc.clone().add(1, 0, -2).getBlock().getType() == Material.COBBLESTONE;
				
				if(C1 && C2 && C3 && C4 && C5 && C6 && C7 && C8 && C9 && C10 && C11 && C12){
					Material M1 = loc.clone().add(1, 0, 0).getBlock().getType();
					Material M2 = loc.clone().add(1, 0, -1).getBlock().getType();
					Material M3 = loc.clone().add(0, 0, 1).getBlock().getType();
					Material M4 = loc.clone().add(-1, 0, 1).getBlock().getType();
					Material M5 = loc.clone().add(-2, 0, 0).getBlock().getType();
					Material M6 = loc.clone().add(-2, 0, -1).getBlock().getType();
					Material M7 = loc.clone().add(0, 0, -2).getBlock().getType();
					Material M8 = loc.clone().add(-1, 0, -2).getBlock().getType();
					
					retVal = "NoMatch";
					
					if(M1 == M2 && M2 == M3 && M3 == M4 && M4 == M5 && M5 == M6 && M6 == M7 && M7 == M8){
						
						retVal = M8.toString();

						return retVal;
					}
				}
			}
		}
		StringUtils.playerMSG(player, "create sigil first!");
		return retVal;
	}
	
	void editSZAdd(Arena arena, Player player){
		
		ArrayList<Location> spawnLocations = arena.getSpawnLocations();
		Location loc = player.getLocation();
		
		for(int i = 0 ; i < spawnLocations.size(); i++){
			if(loc == spawnLocations.get(i)){
				player.sendMessage("Location already set!");
				return;
			}
		}
		
		spawnLocations.add(loc);
		player.sendMessage("Location: " + loc.getX() + ", " + loc.getY() + ", " + loc.getZ() + " set!");
		return;
	}
	
	void editSZRemove(Arena arena, Player player){
		
		ArrayList<Location> spawnLocations = arena.getSpawnLocations();
		Location loc = player.getLocation();
		
		for(int i = 0 ; i < spawnLocations.size(); i++){
			if(loc == spawnLocations.get(i)){
				spawnLocations.remove(loc);
				player.sendMessage("Location: " + loc.getX() + ", " + loc.getY() + ", " + loc.getZ() + " removed!");
				return;
			}
		}	
		
		player.sendMessage("Location is not a spawnzone!");
		return;
	}
}